package myapp;

import org.apache.commons.math3.*;
import org.apache.commons.math3.stat.StatUtils;

public class App
{
	public static void main(String[] args) {

	}

    public static int add(int a, int b) {
       return a + b;
    }

    public static int scale(int a, int b) {
        return a * b;
    }

    public static double range(double[] anArray){

        double sampleMean = StatUtils.mean(anArray);
        double sampleMax = StatUtils.max(anArray);
        double sampleMin = StatUtils.min(anArray);

        return sampleMax - sampleMin;
    }
}
