package myapp;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.ArrayList;

public class Runner {

    @Test
    public void test_d() {

        AppTest instance = new AppTest();
        Class appClass = instance.getClass();

        Result result = JUnitCore.runClasses(appClass);

        //prepare the failed test list
        ArrayList<String> fails = new ArrayList<String>();
        for (Failure failure : result.getFailures()) {
            String str = failure.toString();
            str = str.substring(0, str.indexOf("("));
            fails.add(str);
        }

        //get the test list
        Method[] methods = appClass.getMethods();
        for (int i = 0; i < methods.length; i++) {
            String testName = methods[i].getName();
            boolean isTest = testName.contains("test");
            if(isTest == true){
                if(fails.indexOf(testName) != -1){
                    System.out.println("@fail(Coderpower) : " + testName);
                }else{
                    System.out.println("@pass(Coderpower) : " + testName);
                }

            }
        }


    }
}