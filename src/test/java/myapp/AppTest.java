package myapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


public class AppTest{

   @Test
   public void test_a() {
      int a = 4;
      int b = 4;
      App tools = new App();
      int result = tools.add(a, b);
      assertEquals(8,result);
   }

   @Test
   public void test_b() {
      int a = 4;
      int b = 4;
      App tools = new App();
      int result = tools.scale(a, b);
      assertEquals(16,result);
   }

   @Test
   public void test_c() {
      double[] anArray = {1.9, 2.9, 3.4, 3.5};
      App tools = new App();
      double result = tools.range(anArray);
      assertEquals(1.6, result, 0.0);
   }

}
