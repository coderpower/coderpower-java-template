# Exercise template - Java
This document provides a concise explanation of how content is created on Coderpower. It should also help developers understand the processes behind the scenes of the coderpower platform to evaluate code submissions.

## How it works
### Concepts
Exercises on Coderpower can be Discoveries, Practices or Challenges.

- **Discovery**: An exercise that show, by example, how a library, framework, SDK, API, or design pattern works. It provides an initial code that can be tweaked by developers.
- **Practice**: An exercise that allows developers to explore a subject helping them to understand the concept through practice. Developers can try to solve exercises as many times as they like.
- **Challenge**: An exercise where developers compete against each other to solve a problem. They are assessed on two criteria.
    - **Ranking**:
        - a base score: ( **X** ),
        - a bonus score: ( **Y** ),
        - a bonus time: ( **T** )
            - **1st** participant finding the solution wins **100%** of **X**
            - **2nd** participant wins **80%** of **X**
            - **3rd** wins **70%** of **X**
            - **4th** wins **60%** of **X**
            - **5th** and next win **50%** of **X**
    - **Time**: For those who find the solution in **less** than **T**, we’ll apply the **%** of **T** consumed, to **Y** and add it to their score.


### Imports
Exercises on Coderpower are not more than repositories. The creation process is just a clone of the repository on the server. Then we parse some key information like the written tests, README.md, and the meta.json (we will talk about this one in the next section).

## Requirements
### Testing framework
Behind the scenes, Coderpower validates participations by **running tests**. We have specific testing frameworks regarding the requirements for each language.

For the **java** language we use [junit](http://junit.org/junit4/).

You can use any assertion/mocking library you like but don't forget to add it to your `pom.xml`.


<h3 style="color: red">/!\ Important /!\ </h3>
For performance purposes all codes that take more than **3 seconds** to run will be **killed**. You need to take this into account when you design your test suites, especially for asynchronous code.

### Content
In order to import a repository into the Coderpower platform you need to provide some information.

- `README.md` : The readme will be parsed and taken as the subject of your exercise.
- `meta.json` : The file describes which sources will be editables by the developers. Here is an example :

```json
{
  "editables": [
    "./src/main/java/myapp/App.java"
  ]
}
```

Here we tell to Coderpower this exercise allows the file `App.java` located in `./src/main/java/myapp/` *(path must be relative to the root directory)* to be edited by the developers.
We will then ensure that written tests still pass after developers submit their changes to the files.

## Folders and files
In this section, we will describe the content of this exercise, as an example for creating your own future content.

The repository contains:

- `/src/main/java/myapp/App.java`
- `/src/test/java/myapp/AppTest.java`
- `.gitignore`
- `meta.json`
- `pom.xml`
- `README.md`

#### `App.java`
Here, the sources folder contains all the files needed to make the tests pass.

```java
package myapp;

import org.apache.commons.math3.*;
import org.apache.commons.math3.stat.StatUtils;

public class App
{
	public static void main(String[] args) {

	}

    public static int add(int a, int b) {
       return a + b;
    }

    public static int scale(int a, int b) {
        return a * b;
    }

    public static double range(double[] anArray){

        double sampleMean = StatUtils.mean(anArray);
        double sampleMax = StatUtils.max(anArray);
        double sampleMin = StatUtils.min(anArray);

        return sampleMax - sampleMin;
    }
}

```

#### `AppTest.java`
Here, the test file contains all the tests that will validate the sources.

```java
package myapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class AppTest {

   @Test
   public void test_a() {
      int a = 4;
      int b = 4;
      App tools = new App();
      int result = tools.add(a, b);
      assertEquals(8,result);
   }

   @Test
   public void test_b() {
      int a = 4;
      int b = 4;
      App tools = new App();
      int result = tools.scale(a, b);
      assertEquals(16,result);
   }

   @Test
   public void test_c() {
      double[] anArray = {1.9, 2.9, 3.4, 3.5};
      App tools = new App();
      double result = tools.range(anArray);
      assertEquals(1.6, result, 0.0);
   }

}
```

> **Note:** You should prefix your tests functions by a letter in alphabetical order to respect tests orders. It is useful to match the exercise validation steps defined in Coderpower.


#### `meta.json`
The meta.json file tells where the editable files are.

```json
    {
      "editables": [
        "./src/main/java/myapp/App.java"
      ]
    }
```
> **Note:** You don't need to make all sources editable. It can be only one file, maybe two. Letting developers work with existing code.


#### `README.md`
For demonstration purposes we used the `README.md` to explain how the content creation works, but in real cases, the `README.md` would look like this:

```
# Addition
	You will receive two numbers as parameters and the function has to return the addition of both.
# Scale
    You will receive two numbers as parameters and the function has to return the multiplication of both.
# Range
    You will receive an array of numbers as parameters and the function has to return the array range.
```

If you have any questions regarding the content creation process feel free to contact root@coderpower.com.
