package myapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class AppTest {

	@Test
	public void testPrintHelloWorld() {
		System.out.println("# testPrintHelloWorld");
		assertEquals(App.getHelloWorld(), "Hello World");
	}

    @Test
    public void testPrintHelloWorld2() {
        System.out.println("# testPrintHelloWorld");
        assertEquals(App.getHelloWorld2(), "Hello World 2");
    }

}
